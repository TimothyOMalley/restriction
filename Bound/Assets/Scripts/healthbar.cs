﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthbar : MonoBehaviour
{
	[SerializeField] private Obelisk obl;
	[SerializeField] private RawImage health;

	private void Update(){
		health.transform.localScale = new Vector3(obl.currentHealth/obl.maxHealth,health.transform.localScale.y,health.transform.localScale.z);
		health.transform.localPosition = new Vector3(1.483f-((829-(health.transform.localScale.x*829))/2),health.transform.localPosition.y,health.transform.localPosition.z);
	}
}
