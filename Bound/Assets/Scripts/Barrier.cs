﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    public GameManager spawner;
    public int Zone = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8) Destroy(gameObject);
        spawner.currentZone = Zone;
    }
}
