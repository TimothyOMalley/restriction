﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleAI : MonoBehaviour
{
	[SerializeField] private bool toggled;
	public RawImage swordOrChain;

	private void Awake(){
		swordOrChain = this.gameObject.GetComponent<RawImage>();
	}

	public void Toggle(){
		if(toggled){
			swordOrChain.gameObject.SetActive(false);
			toggled = false;
		}
		else{
			swordOrChain.gameObject.SetActive(true);
			toggled = true;
		}
	}
}
