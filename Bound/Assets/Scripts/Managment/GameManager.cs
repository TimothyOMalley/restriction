﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public enum GameState {setup, gameLoop, gamePause, end}

	public GameObject[] ZoneOne;
    public GameObject[] ZoneTwo;
    public GameObject[] ZoneThree;
    private GameObject[] livingEnemies;
	[SerializeField] private List<GameObject> enemies;
	public float spawnDelay = 5f;
	public float timer = 5f;
	private GameState gameState = GameState.setup;
    public int currentZone = 1;

	public void TogglePause(){
		if(gameState == GameState.gameLoop){
			gameState = GameState.gamePause;
			livingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
			for(int i=0;i<livingEnemies.Length;i++){
				livingEnemies[i].SetActive(false);
			}
		}
		else{
			gameState = GameState.gameLoop;
			for(int i=0;i<livingEnemies.Length;i++){
				livingEnemies[i].SetActive(true);
			}
		}
	}
	private void Update(){

		switch(gameState){

			case GameState.setup:
				State_Setup();
				break;
			case GameState.gameLoop:
				State_GameLoop();
				break;
			case GameState.end:
				State_End();
				break;
		}

		void State_Setup(){
			gameState = GameState.gameLoop;
		}

		void State_GameLoop(){
            GameObject spawnLocation;
            switch(currentZone)
            {
                case 1:
                    {
                        spawnLocation = ZoneOne[Random.Range(0, ZoneOne.Length)];
                        break;
                    }
                case 2:
                    {
                        spawnLocation = ZoneTwo[Random.Range(0, ZoneTwo.Length)];
                        break;
                    }
                default:
                    {
                        spawnLocation = ZoneThree[Random.Range(0, ZoneThree.Length)];
                        break;
                    }
            }
			timer -= Time.deltaTime;
			if(timer <= 0)
            {
                int r = Random.Range(0, 15);
                if (r < 9)
                {
                    Instantiate(enemies[0], spawnLocation.transform.position, spawnLocation.transform.rotation);
                    Debug.Log("Spawned Shadow Wolf");
                }
                else if (r < 14)
                {
                    Debug.Log("Spawned Void Thrower");
                    Instantiate(enemies[1], spawnLocation.transform.position, spawnLocation.transform.rotation);
                }
                else
                {
                    Debug.Log("Spawned Jumper");
                    Instantiate(enemies[2], spawnLocation.transform.position, spawnLocation.transform.rotation);
                }
                timer = spawnDelay;
			}


		}

		void State_End(){

		}
	}

}
