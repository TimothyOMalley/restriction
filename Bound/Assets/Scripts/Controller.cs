﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    protected Pawn possessedPawn;
    [SerializeField] protected Pawn startingPawn;

    private void Start()
    {
        PossessPawn(startingPawn);
    }

    public virtual void PossessPawn(Pawn newPawn)
    {
        if (possessedPawn != null)
        {
            possessedPawn.currentController = null;
            possessedPawn.OnUnpossess();
        }
        possessedPawn = newPawn;
        possessedPawn.currentController = this;
        possessedPawn.OnPossess();
    }
}
