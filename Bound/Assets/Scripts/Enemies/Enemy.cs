﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
    public GameObject player;
    public Obelisk obelisk;
    [SerializeField] protected float health = 5f;
    protected NavMeshAgent agent;

    void Start() {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("player");
        obelisk = GameObject.FindObjectOfType<Obelisk>();
    }


    private void Update() {
        if (Vector3.Distance(transform.position, player.transform.position) > 2f)
            agent.destination = player.transform.position;
        else
            agent.ResetPath();
    }

    public virtual void TakeDamage(float damage) {
        health -= damage;
        if (health <= 0)
            OnDeath();
    }

    public virtual void Attack() {

    }

    public virtual void OnDeath() {
        Destroy(gameObject);
    }
}
