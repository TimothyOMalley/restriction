﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Thrower : Enemy
{
    [SerializeField] Transform spawnPosition;
    [SerializeField] GameObject projectile;
    private bool Moving = true;
    [SerializeField] float fireCooldown = 3f;
    
    private float cooldown;
    RaycastHit hit;

    private void Start()
    {
        cooldown = fireCooldown;
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
        obelisk = GameObject.Find("Obelisk").GetComponent<Obelisk>();
        agent.destination = ((transform.position - obelisk.gameObject.transform.position).normalized * (obelisk.chainMaxLength -3)) + obelisk.transform.position;
    }

    private void Update()
    {
        if (Vector3.Distance(agent.destination, transform.position) < 1f)
        {
            agent.ResetPath();
            Moving = false;
        }

        if (Vector3.Distance(transform.position, player.transform.position) < player.GetComponent<Player>().GetChainLength())
            agent.ResetPath();

        if (Physics.Raycast(transform.position, (obelisk.gameObject.transform.position - transform.position).normalized, out hit) && !Moving)
        {
            if (hit.collider.gameObject == obelisk.gameObject || hit.collider.gameObject == projectile)
            {
                Attack();
            }
            else
            {
                Vector3 newDestination = transform.position - obelisk.gameObject.transform.position;
                newDestination = Quaternion.Euler(0, -15, 0) * newDestination;
                newDestination += obelisk.transform.position;

                agent.SetDestination(newDestination);
                Moving = true;
            }
        }
    }

    public override void Attack()
    {
        spawnPosition.LookAt(obelisk.transform);
        cooldown -= Time.deltaTime;
        if (cooldown <= 0)
        {
            cooldown = fireCooldown;
            Instantiate(projectile, spawnPosition.position, spawnPosition.rotation);
        }
    }
}