﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public float speed = 3.0f;
    public float dmg = 10f;

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GetComponent<Obelisk>())
        {
            other.gameObject.GetComponent<Obelisk>().TakeDamage(dmg);
            Destroy(this.gameObject);
        }
    }
}
