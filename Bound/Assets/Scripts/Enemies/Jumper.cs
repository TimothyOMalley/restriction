﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Jumper : Enemy {
    private bool Moving = true;
    private bool Attacking = false;
    private bool Jumping = false;
    [SerializeField] private float jumpHeight = 5f;
    [SerializeField] private float jumpTime = 5f;
    [SerializeField] private float jumpRadius = 8f;
    private float jumpStartTime;
    private RaycastHit hit;


    private void Start() {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
        obelisk = GameObject.Find("Obelisk").GetComponent<Obelisk>();
        agent.destination = ((transform.position - obelisk.gameObject.transform.position).normalized * (obelisk.chainMaxLength)) + obelisk.transform.position;

    }

    void Update() {
        if (!Attacking) {
            if (Vector3.Distance(agent.destination, transform.position) < 2f) {
                agent.ResetPath();
                Moving = false;
            }
            if (Vector3.Distance(player.transform.position, transform.position) > obelisk.chainMaxLength + 2) {
                Moving = true;
                agent.destination = ((transform.position - obelisk.gameObject.transform.position).normalized * (obelisk.chainMaxLength)) + obelisk.transform.position;
            }

            if (Physics.Raycast(transform.position, (player.transform.position - transform.position).normalized, out hit) && !Moving) {
                if (hit.collider.gameObject == player && Vector3.Distance(player.transform.position, transform.position) < jumpRadius) {
                    Attack();
                }
                else {
                    Vector3 newDestination = transform.position - player.transform.position;
                    newDestination = Quaternion.Euler(0, 95, 0) * newDestination;
                    newDestination += player.transform.position;

                    agent.SetDestination(newDestination);
                    Moving = true;
                }
            }
        }
        else {
            if (Jumping) {
                float y = jumpHeight * Mathf.Sin(Mathf.PI * ((Time.time - jumpStartTime) / jumpTime));
                transform.position = Vector3.Lerp(transform.position, player.transform.position, (Time.time - jumpStartTime) / jumpTime);
                transform.position = new Vector3(transform.position.x, y, transform.position.z);
                if (Time.time - jumpStartTime > jumpTime)
                    Jumping = false;
            }
        }
    }

    public override void Attack() {
        agent.ResetPath();
        jumpStartTime = Time.time;
        Attacking = true;
        Jumping = true;
        player.GetComponent<Player>().Stun(gameObject);
    }

    public override void OnDeath() {
        player.GetComponent<Player>().EndStun(gameObject);
        base.OnDeath();
    }
}
