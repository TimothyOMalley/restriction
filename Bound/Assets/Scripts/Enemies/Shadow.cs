﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Shadow : Enemy
{
    [SerializeField] private float damage = 8f;
    private bool Moving = true;
    private bool targetingObelisk = false;
    private bool attacking = false;
    private float deathTimer = 0f;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
        obelisk = GameObject.Find("Obelisk").GetComponent<Obelisk>();
        agent.destination = ((transform.position - obelisk.gameObject.transform.position).normalized * (obelisk.chainMaxLength + 1)) + obelisk.transform.position;
    }

    private void Update()
    {
        if (Vector3.Distance(agent.destination, transform.position) < 0.5f)
        {
            agent.ResetPath();
            Moving = false;
        }

        if (attacking)
        {
            deathTimer += Time.deltaTime;
            if (deathTimer > 1.25f) Attack();
        }

        if (!Moving)
        {
            if(Random.Range(0f,1f) > 0.9f)
            {
                agent.SetDestination(obelisk.transform.position);
                Moving = true;
                targetingObelisk = true;
            }
            else
            {
                Vector3 newDestination = transform.position - obelisk.gameObject.transform.position;
                newDestination = Quaternion.Euler(0, Random.Range(-15, 15), 0) * newDestination;
                newDestination += obelisk.transform.position;

                agent.SetDestination(newDestination);
                Moving = true;
            }
        }
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
    }

    public override void Attack()
    {
        obelisk.TakeDamage(damage);
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject == obelisk.gameObject) attacking = true;
    }
}
