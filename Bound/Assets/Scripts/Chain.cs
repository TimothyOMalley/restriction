﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chain : MonoBehaviour
{//4
    public Transform A;
    public Transform B;
    public Obelisk _obelisk;
    public List<Renderer> links;
    private float obeliskModifier;
    private float chainLength;

    private void Start()
    {
        chainLength = Vector3.Distance(A.position, B.position);
    }

    void Update()
    {
        obeliskModifier = (_obelisk.chainMaxLength - chainLength) / (_obelisk.chainMaxLength - _obelisk.chainMinLength);

        transform.position = A.position;
        transform.localScale = new Vector3(2, 2, Vector3.Distance(A.position, B.position) / 4.2f);
        transform.LookAt(B);

        chainLength = Vector3.Distance(A.position, B.position);
        if (chainLength > _obelisk.chainMinLength)
        {
            foreach(Renderer link in links)
            {
                link.material.color = new Color(link.material.color.r, link.material.color.g, link.material.color.b, 1 - obeliskModifier);
            }
        }
        else
        {
            foreach (Renderer link in links)
            {
                link.material.color = new Color(link.material.color.r, link.material.color.g, link.material.color.b, 0);
            }
        }
    }
}
