﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{
    [SerializeField] private EscapeMenu menuPawn;
    [SerializeField] public Player player;
    [SerializeField] private GameManager pauseGame;
    [SerializeField] private GameObject esc;

    private void Update()
    {
        if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            possessedPawn.Movement(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }

        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
        {
            possessedPawn.Rotate(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        }

        if(Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            possessedPawn.Scroll(Input.GetAxis("Mouse ScrollWheel"));
        }

        if (Input.GetButtonDown("LeftClick"))
        {
            possessedPawn.LeftClick();
        }

        if (Input.GetButtonDown("RightClick"))
        {
            possessedPawn.RightClick();
        }

        if (Input.GetButtonDown("Jump"))
        {
            possessedPawn.SpaceBar();
        }

        if (Input.GetButtonDown("Shift"))
        {
            possessedPawn.Shift();
        }

        if (Input.GetButton("E"))
        {
            possessedPawn.EPress();
        }

        if (Input.GetButton("Q"))
        {
            possessedPawn.QPress();
        }

        if (Input.GetButtonDown("V"))
        {
            possessedPawn.VPress();
        }

        if (Input.GetButtonDown("Cancel"))
        {
            pauseGame.TogglePause();
            esc.gameObject.GetComponent<EscapeMenu>().toggleMenu();
            if(possessedPawn == menuPawn)
            {
                PossessPawn(player);
            }
            else if(possessedPawn == player)
            {
                PossessPawn(menuPawn);
            }

        }
    }
}
