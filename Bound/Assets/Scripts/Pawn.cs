﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{
    public Controller currentController;
    [SerializeField] protected bool showDebug = false;

    public virtual void OnPossess()
    {

    }

    public virtual void OnUnpossess()
    {

    }

    public virtual void Movement(float horizontal, float vertical)
    {
        if(showDebug)Debug.Log("Movement | X: " + horizontal + " Y: " + vertical);
    }

    public virtual void Rotate(float horizontal, float vertical)
    {
        if (showDebug) Debug.Log("Rotation | X: " + horizontal + " Y: " + vertical);
    }

    public virtual void Scroll(float mouseWheel)
    {
        if (showDebug) Debug.Log("Scroll Wheel: " + mouseWheel);
    }

    public virtual void LeftClick()
    {
        if (showDebug) Debug.Log("LeftClick Pressed.");
    }

    public virtual void RightClick()
    {
        if (showDebug) Debug.Log("RightClick Pressed.");
    }

    public virtual void SpaceBar()
    {
        if (showDebug) Debug.Log("SpaceBar Pressed.");
    }

    public virtual void Shift()
    {
        if (showDebug) Debug.Log("Shift Pressed.");
    }

    public virtual void EPress()
    {
        if (showDebug) Debug.Log("E Pressed.");
    }

    public virtual void QPress()
    {
        if (showDebug) Debug.Log("Q Pressed.");
    }

    public virtual void VPress()
    {
        if (showDebug) Debug.Log("V Pressed.");
    }

    public virtual void Escape()
    {
        if (showDebug) Debug.Log("Escape Pressed.");
    }
}
