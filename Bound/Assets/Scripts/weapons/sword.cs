﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sword : MonoBehaviour
{
	private float dmgM;

    public void SetDamageMultiplyer(float dmg){dmgM = dmg;}

	    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            other.gameObject.GetComponent<Enemy>().TakeDamage(3);
        }
    }
}
