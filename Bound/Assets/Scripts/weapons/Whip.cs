﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whip : MonoBehaviour
{
    private float dmg = 0f;

    public void SetDamageMultiplyer(float dmgM){ dmg = dmgM; }

    void Awake(){
        
    }

    void Update()
    {
    	//print(gameObject.transform.forward.x);
        print(dmg);
        gameObject.transform.localScale = new Vector3(1,2+(2*(1-dmg)),1);
    	if(gameObject.transform.localRotation.x+(gameObject.transform.localRotation.z) >= 0.9){
    		Destroy(gameObject);
    	}
        gameObject.transform.Rotate(8, 0, 0);
        print(1*dmg);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
             other.gameObject.GetComponent<Enemy>().TakeDamage(10*dmg);
        }
    }
}
