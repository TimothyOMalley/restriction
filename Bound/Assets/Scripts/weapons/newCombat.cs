﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newCombat : MonoBehaviour
{
	[SerializeField] private Vector3 offset;
	private bool toggled;
	public GameObject projectile;
	public GameObject whip;
	public GameObject sword;
	public GameObject prntObj;
	private GameObject onlySword;
	public GameObject spawnAt;
	private float mlt = 1f;
	private float dmg;

    [SerializeField] float fireCooldown = 0.5f;
    private float cooldown = 0;

    public void SetDamageMlt(float mlt){ dmg = mlt; }

    private void Update()
    {
        cooldown -= Time.deltaTime;
    }

    public void Fire()
	{
        GameObject bullet;

        if (cooldown < 0)
        {
            bullet = Instantiate(projectile, prntObj.transform.position + offset, gameObject.transform.rotation);
            bullet.gameObject.GetComponent<Shoot>().SetDamageMultiplyer(dmg);
            cooldown = fireCooldown;
        }
	}

    public void Whip()
	{
		//Instantiate(whip, prntObj.transform.position + new Vector3(-0.0645f,0.781f,0.4409f), gameObject.transform.rotation);
		GameObject inWhip = Instantiate(whip, prntObj.transform);
		inWhip.transform.position = spawnAt.transform.position + new Vector3(0f,-0.32f,0f);
		inWhip.gameObject.GetComponent<Whip>().SetDamageMultiplyer(dmg);
	}

	public void DeleteSword(){
		if(toggled){
			Destroy(onlySword);
			toggled = false;
		}
	}

	public void Sword(){
		if(toggled){
			Destroy(onlySword);
			toggled = false;
		}
		else{
			onlySword = Instantiate(sword, prntObj.transform);
			onlySword.transform.position = spawnAt.transform.position;
			onlySword.gameObject.GetComponent<sword>().SetDamageMultiplyer(dmg);
			toggled = true;
		}

	}
}