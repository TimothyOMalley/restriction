﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
	public float speed = 5.0f;
    private float dmg = 0f;

    public void SetDamageMultiplyer(float dmgM){ dmg = dmgM; }
	
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

        private void OnTriggerEnter(Collider other)
    {

        if(other.tag == "Enemy")
        {
        	other.gameObject.GetComponent<Enemy>().TakeDamage(5*dmg);
            Destroy(this.gameObject);
        }
    }
}