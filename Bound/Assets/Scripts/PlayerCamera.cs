﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField] GameObject player;

    public void Rotate(float yRotation)
    {
        transform.Rotate(new Vector3(0, yRotation * Time.deltaTime * 100, 0));
    }

    void Update()
    {
        transform.position = player.transform.position;
    }
}
