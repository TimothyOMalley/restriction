﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Obelisk : MonoBehaviour
{
    [SerializeField] public float maxHealth;
    [SerializeField] private GameObject player;
    public Rigidbody rb;
    public float currentHealth;
    public float chainMaxLength = 15f;
    public float chainMinLength = 4f;

    protected void Start()
    {
        if (!rb) rb = GetComponent<Rigidbody>();
        currentHealth = maxHealth;
    }

    public void TakeDamage(float damage)
	{
        currentHealth -= damage;
        if(currentHealth <= 0)
		{
            SceneManager.LoadScene("Dead Screen");
		}
	}
}
