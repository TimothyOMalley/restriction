﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscapeMenu : Pawn
{
	private enum ActiveImage {pause, controls, story}

	private bool UIMenus = true; //true = User Interface, false = Escape Menu
	[SerializeField] Canvas userInterface;
	private Canvas escapeMenu;
	[SerializeField] private PlayerController controller;
	[SerializeField] private GameManager pauseGame;
	[SerializeField] private GameObject pause;
	[SerializeField] private GameObject controls;
	//[SerializeField] private GameObject story;

	private void Awake(){
		escapeMenu = this.gameObject.GetComponent<Canvas>();
		escapeMenu.gameObject.SetActive(false);
	}

	private void OnEnable(){
		pauseScreen();
	}

	public void toggleMenu(){
		if(UIMenus == true){
			escapeMenu.gameObject.SetActive(true);
			userInterface.gameObject.SetActive(false);
			UIMenus = false;
		}
		else{
			escapeMenu.gameObject.SetActive(false);
			userInterface.gameObject.SetActive(true);
			UIMenus = true;
		}
	}
	public void Resume(){
		pauseGame.TogglePause();
		toggleMenu();
		controller.PossessPawn(controller.player);

	}

	public void pauseScreen(){
		pause.gameObject.SetActive(true);
		controls.gameObject.SetActive(false);
	}

	public void controlsScreen(){
		pause.gameObject.SetActive(false);
		controls.gameObject.SetActive(true);
	}

	/*public void storyScreen(){
		pause.gameObject.SetActive(false);
		controls.gameObject.SetActive(false);
		story.gameObject.SetActive(true);
	}*/

}
