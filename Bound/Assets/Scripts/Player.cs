using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : Pawn {
    [SerializeField] private float maxAccelerationSpeed = 1f;
    [SerializeField] private float maxSpeed = 30f;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private GameObject playerCamera;
    [SerializeField] private PlayerCamera cameraCenter;
    [SerializeField] private GameObject obelisk;
    [SerializeField] private newCombat combat;
    [SerializeField] private ToggleAI UIToggle; //user interface not AI
    [SerializeField] private ToggleAI UIToggle2;
    [SerializeField] float obeliskModifier = 1;
    private Camera _playerCamera;
    private Obelisk _obelisk;
    private RaycastHit hit;
    private Ray ray;
    private float cooldown = 0f;
    private float chainLength = 0f;
    private float accelerationSpeed = 1f;
    private float speed = 1f;
    private bool swordOrWhip = true;
    private float scrollWheelMin = -4f;
    private float scrollWheelMax = 10f;
    private float scrollWheelScroll = 0f;
    [SerializeField] GameObject _stunner;
    [SerializeField] bool stunned = false;
    [SerializeField] int stunClicks = 0;

    public float GetChainLength() {
        return chainLength;
    }
    void Start() {
        speed = maxSpeed;
        accelerationSpeed = maxAccelerationSpeed;
        _playerCamera = playerCamera.GetComponent<Camera>();
        _obelisk = obelisk.GetComponent<Obelisk>();
        if (rb == null) {
            if (GetComponent<Rigidbody>())
                rb = GetComponent<Rigidbody>();
            else
                rb = gameObject.AddComponent<Rigidbody>();

            rb.freezeRotation = true;
        }
    }

    void Update() {
        combat.SetDamageMlt(obeliskModifier);

        if (cooldown > 0) {
            cooldown -= Time.deltaTime;
        }
        if (cooldown < 0) {
            cooldown = 0;
        }

        obeliskModifier = ((_obelisk.chainMaxLength - chainLength) / (_obelisk.chainMaxLength - _obelisk.chainMinLength));

        chainLength = Vector3.Distance(transform.position, obelisk.transform.position);
        if (chainLength > _obelisk.chainMinLength) {
            speed = (maxSpeed * obeliskModifier) + 0.1f;
            accelerationSpeed = (maxAccelerationSpeed * obeliskModifier) + 0.1f;
            if (speed < 0.1f)
                speed = 0.1f;
            if (accelerationSpeed < 0.1f)
                accelerationSpeed = 0.1f;
        }
        else {
            speed = maxSpeed;
        }

        if (chainLength > _obelisk.chainMaxLength * 0.88f)
            _obelisk.rb.velocity = (transform.position - obelisk.transform.position).normalized * 5;



        //Rotates the player to face mouse position --------------------------------------------------------------------
        ray = playerCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit)) {
            transform.rotation = Quaternion.Slerp(
            transform.rotation,
            Quaternion.LookRotation(new Vector3(hit.point.x - transform.position.x, 0, hit.point.z - transform.position.z)),
            0.15F);
        }
        else {
            transform.rotation = Quaternion.Slerp(
            transform.rotation,
            Quaternion.LookRotation(new Vector3(rb.velocity.x - transform.position.x, 0, rb.velocity.z - transform.position.z)),
            0.15F);
        }
        //--------------------------------------------------------------------------------------------------------------
    }

    public void Stun(GameObject stunner) {
        stunned = true;
        if(stunner != null)
            _stunner = stunner;
        stunClicks = 0;
    }

    public override void Movement(float horizontal, float vertical) {
        if (!stunned) {
            //Get player input and multiply by max speed
            Vector3 inputVelocity = new Vector3(horizontal, 0, vertical) * speed;
            inputVelocity = Quaternion.Euler(0, cameraCenter.transform.eulerAngles.y, 0) * inputVelocity;

            //Clamp the players speed from going past the maximum
            Vector3 velocityTotal = inputVelocity - rb.velocity;
            velocityTotal.x = Mathf.Clamp(velocityTotal.x, -speed, speed);
            velocityTotal.z = Mathf.Clamp(velocityTotal.z, -speed, speed);
            velocityTotal.y = 0;

            //Normalize and apply the calculated force
            velocityTotal = velocityTotal.normalized;
            rb.AddForce(velocityTotal * Time.deltaTime * 500 * accelerationSpeed);
        }

        base.Movement(horizontal, vertical);
    }

    public override void Rotate(float horizontal, float vertical) {
        base.Rotate(horizontal, vertical);
    }

    public override void Scroll(float mouseWheel) {
        if (mouseWheel > 0) {
            if (scrollWheelScroll < scrollWheelMax) {
                playerCamera.transform.position += playerCamera.transform.forward * 0.1f;
                scrollWheelScroll += 0.1f;
            }
        }
        else if (mouseWheel < 0) {
            if (scrollWheelScroll > scrollWheelMin) {
                playerCamera.transform.position -= playerCamera.transform.forward * 0.1f;
                scrollWheelScroll -= 0.1f;
            }
        }
        base.Scroll(mouseWheel);
    }

    public override void LeftClick() {
        if (!stunned) {
            combat.Fire();
            base.LeftClick();
        }
        else {
            if (_stunner == null)
                stunned = false;
            stunClicks++;
            if (stunClicks >= 15) {
                stunned = false;
                if(_stunner != null)
                    Destroy(_stunner);
            }
        }
    }

    public void EndStun(GameObject stunner) {
        if (_stunner == stunner) {
            stunned = false;
            stunClicks = 0;
        }
    }

    public override void RightClick() {
        if (swordOrWhip) {
            combat.Sword();
        }
        else {
            if (cooldown == 0) {
                combat.Whip();
                cooldown = .38f;
            }
        }
        base.RightClick();
    }

    public override void Shift() {
        base.Shift();
    }

    public override void SpaceBar() {
        base.SpaceBar();
    }

    public override void EPress() {
        cameraCenter.Rotate(1);
        base.EPress();
    }

    public override void QPress() {
        cameraCenter.Rotate(-1);
        base.QPress();
    }

    public override void VPress() {
        if (swordOrWhip) {
            swordOrWhip = false;
            combat.DeleteSword();
        }
        else { swordOrWhip = true; }

        UIToggle.Toggle();
        UIToggle2.Toggle();


        base.VPress();
    }

    public override void Escape() {
        base.Escape();
    }
}
